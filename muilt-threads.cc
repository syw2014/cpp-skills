/*************************************************************************
# File Name: muilt-threads.cc
# Method: 
# Author: Jerry Shi
# Mail: jerryshi0110@gmail.com
# Created Time: 2016年01月02日 星期六 14时26分03秒
 ************************************************************************/
// complie : g++ -std=c++11 filename -lpthread
// 该程序根据 cpp concurrency in action 编写
//  section 2 page 
#include <iostream>
#include <algorithm>
#include <thread>
#include <vector>

void hello(void)
{
    std::cout << "Hellp Concurrent world!\n";
}

void do_work(int id){
    std::cout << "My Index ===> " << id << std::endl;
}

void f(unsigned int thread_nm){
    std::vector<std::thread> threads;
    for(unsigned i = 0; i < thread_nm; ++i){
        threads.push_back(std::thread(do_work,i));
    }

    std::for_each(threads.begin(),threads.end(),std::mem_fn(&std::thread::join));
}

int main(int argc, char* argv[])
{
    std::thread t(hello);
    t.join();
    unsigned nm = 22;
    f(22);
    
    return 0;
}
